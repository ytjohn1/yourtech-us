---
title: Firefox Containers
author: ytjohn
date: 2018-04-18

layout: post
---

I've been using container tabs with firefox for a few weeks. it's awesome.  I have a container called "Personal" that is logged into Google. None of my other containers are, including the default.
I have containers for "Work", "Banking", and "Shopping". I have a container for "Reddit" that I use when browsing reddit and any links I open from that. It takes a while to acclimate to. I do have to remember not to log into gmail on the default tab, and I have to think about where I'm opening a new site. But as I go along, I tell Firefox to always open in [this] container. I'm also using the [Facebook Container Extension](https://blog.mozilla.org/firefox/facebook-container-extension/) which restricts all Facebook domains to one container. 

As far as information crossover, currently Google and Facebook know all sorts of things about me. But the longer I use this, the more I separate my browsing into containers, the more fragmented and incomplete that picture becomes. If my bank puts a Facebook tracking icon on their page, that tracking information will be lacking my details and remain in a separate bucket than the tracking information they get from when I'm browsing reddit. 

